
#include <Stepper.h>

#define STEPS 32

//stepper 1
#define s1IN1 6
#define s1IN2 5
#define s1IN3 4
#define s1IN4 3

Stepper stepper1(STEPS, s1IN4, s1IN2, s1IN3, s1IN1);

//stepper2
#define s2IN1 11
#define s2IN2 10
#define s2IN3 9
#define s2IN4 8

Stepper stepper2(STEPS, s2IN4, s2IN2, s2IN3, s2IN1);

//stepper3
#define s3IN1 33
#define s3IN2 32
#define s3IN3 31
#define s3IN4 30

Stepper stepper3(STEPS, s3IN4, s3IN2, s3IN3, s3IN1);

//stepper4
#define s4IN1 45
#define s4IN2 44
#define s4IN3 43
#define s4IN4 42

Stepper stepper4(STEPS, s4IN4, s4IN2, s4IN3, s4IN1);

// joystick1
#define joystick1X A0
#define joystick1Y A1

// joystick2
#define joystick2X A5
#define joystick2Y A6


// mode button and flag (false => manual, true => auto)
int button = 12;
volatile boolean mode = true;

// bluetooth String 
String message = "";
boolean received = false;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  Serial1.begin(9600);
  pinMode(button, INPUT_PULLUP);
}

void loop() {
  // put your main code here, to run repeatedly:

  int j1X = analogRead(joystick1X);
  int j1Y = analogRead(joystick1Y);

  int j2X = analogRead(joystick2X);
  int j2Y = analogRead(joystick2Y);

  if(button = 0){
    if(mode == true){
      mode = false;
    }
    else{
      mode = true;
    }
  }

  if( mode == true ){

    // comenzi bluetooth!!
    
    while(message.length() < 5){
      bluetoothEvent();
    }
    
    if(message.compareTo("J1XG") > 0){
     
      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);

      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);

      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper1G();
      }
      
    }
    else if(message.compareTo("J1XL")){

      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);

      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);

      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper1L();
      }
      
    }
    else if(message.compareTo("J1YG") > 0){

      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);
      
      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);

      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper2G();
      }
      
    }
    else if(message.compareTo("J1YL") > 0){

      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);

      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);

      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper2L();
      }
      
    }
    else if(message.compareTo("J2XG") > 0){

      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);

      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);

      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper3G();
      }
      
    }
    else if(message.compareTo("J2XL") > 0){

      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);

      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);

      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper3L();
      }
      
    }
    else if(message.compareTo("J2YG") > 0){

      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);

      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);

      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper4G();
      }
      
    }
    else if(message.compareTo("J2YL") > 0){

      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);

      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);

      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);
      
      for(int i = 0; i < 5; i++){
        moveStepper4L();
      }
    }
    else{
      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);

      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);

      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);

      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
    }
    Serial.println("" + message);
    message = "";
  }
  else{
    // comenzi joystick-uri
    
    if( j1X > 450 && j1X < 523){
      digitalWrite(s1IN1, LOW);
      digitalWrite(s1IN2, LOW);
      digitalWrite(s1IN3, LOW);
      digitalWrite(s1IN4, LOW);
    }
    else{
  
      while(j1X <= 450){

        int speed_ = map(j1X, 500, 0, 5, 500);
        stepper1.setSpeed(speed_);
        stepper1.step(-1);
        j1X = analogRead(joystick1X);
      }
  
      while(j1X >= 523){

        int speed_ = map(j1X, 523, 1023, 5, 500);
        stepper1.setSpeed(speed_);
        stepper1.step(1);
        j1X = analogRead(joystick1X);
      }
      
    }
  
    if( j1Y > 450 && j1Y < 523){
      digitalWrite(s2IN1, LOW);
      digitalWrite(s2IN2, LOW);
      digitalWrite(s2IN3, LOW);
      digitalWrite(s2IN4, LOW);
    }
    else{
  
      while(j1Y <= 450){

        int speed_ = map(j1Y, 500, 0, 5, 500);
        stepper2.setSpeed(speed_);
        stepper2.step(-1);
        j1Y = analogRead(joystick1Y);
      }
  
      while(j1Y >= 523){

        int speed_ = map(j1Y, 523, 1023, 5, 500);
        stepper2.setSpeed(speed_);
        stepper2.step(1);
        j1Y = analogRead(joystick1Y);
      }
      
    }
  
    if( j2X > 490 && j2X < 523){
      digitalWrite(s3IN1, LOW);
      digitalWrite(s3IN2, LOW);
      digitalWrite(s3IN3, LOW);
      digitalWrite(s3IN4, LOW);
    }
    else{
  
      while(j2X <= 490){

        int speed_ = map(j2X, 490, 0, 5, 500);
        stepper3.setSpeed(speed_);
        stepper3.step(-1);
        j2X = analogRead(joystick2X);
      }
  
      while(j2X >= 523){

        int speed_ = map(j2X, 523, 1023, 5, 500);
        stepper3.setSpeed(speed_);
        stepper3.step(1);
        j2X = analogRead(joystick2X);
      }
      
    }
  
    if( j2Y > 490 && j2Y < 523){
      digitalWrite(s4IN1, LOW);
      digitalWrite(s4IN2, LOW);
      digitalWrite(s4IN3, LOW);
      digitalWrite(s4IN4, LOW);
    }
    else{
  
      while(j2Y <= 490){

        int speed_ = map(j2Y, 490, 0, 5, 500);
        stepper4.setSpeed(speed_);
        stepper4.step(-1);
        j2Y = analogRead(joystick2Y);
      }
  
      while(j2Y >= 523){

        int speed_ = map(j2Y, 523, 1023, 5, 500);
        stepper4.setSpeed(speed_);
        stepper4.step(1);
        j2Y = analogRead(joystick2Y);
      }
      
    }
   }
}

void bluetoothEvent(){

  while(Serial1.available()){
    
    char ch = (char)Serial1.read();
    if(ch != '\n'){
      message += ch;
    }
    
    if(ch == '\n'){
      received = true;
    }
  }
}

void moveStepper1G(){
  int speed_ = map(923, 523, 1023, 5, 500);
  stepper1.setSpeed(speed_);
  stepper1.step(1);
}

void moveStepper1L(){
  int speed_ = map(15, 500, 0, 5, 500);
   stepper1.setSpeed(speed_);
   stepper1.step(-1);
}

void moveStepper2G(){
  int speed_ = map(923, 523, 1023, 5, 500);
  stepper2.setSpeed(speed_);
  stepper2.step(1);
}

void moveStepper2L(){
  int speed_ = map(15, 500, 0, 5, 500);
  stepper2.setSpeed(speed_);
  stepper2.step(-1);
}

void moveStepper3G(){
  int speed_ = map(923, 523, 1023, 5, 500);
  stepper3.setSpeed(speed_);
  stepper3.step(1);
}

void moveStepper3L(){
  int speed_ = map(15, 490, 0, 5, 500);
  stepper3.setSpeed(speed_);
  stepper3.step(-1);
}

void moveStepper4G(){
  int speed_ = map(923, 523, 1023, 5, 500);
  stepper4.setSpeed(speed_);
  stepper4.step(1);
}

void moveStepper4L(){
  int speed_ = map(15, 490, 0, 5, 500);
  stepper4.setSpeed(speed_);
  stepper4.step(-1);
}
